#!/bin/bash

# Create the analysis_plots folder
mkdir analysis_plots

# Create the subfolders within analysis_plots
mkdir analysis_plots/boxplots
mkdir analysis_plots/comparison
mkdir analysis_plots/confusion_matrix
mkdir analysis_plots/linear_plots
mkdir analysis_plots/field_to_field_comparison


# Optionally, you can also add some confirmation messages
echo "Folders created successfully:"
echo "- analysis_plots"
echo "- analysis_plots/boxplots"
echo "- analysis_plots/comparison"
echo "- analysis_plots/confusion_matrix"
echo "- analysis_plots/linear_plots"
echo "- analysis_plots/field_to_field_comparison"
 

