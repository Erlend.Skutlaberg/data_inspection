import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from ML_components.job_dataset import JobDataset
from ML_components.stateful_model import StatefulTransformer
import data_management.job_data_handler as jdh
from datetime import datetime as dt
from visualizer import visualizer
import matplotlib.pyplot as plt


def format_seconds(seconds):
    hours, remainder = divmod(seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    return "{:02}:{:02}:{:02}".format(int(hours), int(minutes), int(seconds))


# Collect job attributes
slurm, self_nodes, system_nodes = jdh.get_job_attributes_with_corresponding_site_data()
system_and_slurm_attr = jdh.combine_attributes(slurm, system_nodes)
done_attr, err_attr = jdh.split_attr_on_end_status(system_and_slurm_attr, 5)

# err_attr = err_attr[:50]

# Create training dataset
error_to_done_ratio = 1
done_attr = done_attr[0 : len(err_attr) * error_to_done_ratio]

# Extract validation set
val_job_amount = len(err_attr) // 5


# Extract validation attributes
val_attr = done_attr[0:val_job_amount] + err_attr[0:val_job_amount]
done_attr = done_attr[val_job_amount:]
err_attr = err_attr[val_job_amount:]

# Combine the done and error attributes
training_attr = done_attr + err_attr
print(f"Size of training dataset: {len(training_attr)}")
print(f"Size of validation dataset: {len(val_attr)}")


site_buckets = ["slurm", "system_nodes"]
input_size = 8

# Create train and validation datasets
train_job_dataset = JobDataset(
    training_attr, buckets=site_buckets, input_size=input_size
)
val_job_dataset = JobDataset(val_attr, buckets=site_buckets, input_size=input_size)

# Create train and validation dataloaders
train_job_dataloader = DataLoader(
    train_job_dataset, batch_size=1, shuffle=True
)  # Set batch_size to 1 to load one job at a time
val_job_dataloader = DataLoader(val_job_dataset, batch_size=1, shuffle=True)


# Initialize the model
model_dim = input_dim  # Dimension of the model
num_heads = 4  # Number of heads in the multi-head attention mechanism
num_layers = 4  # Number of sub-encoder-layers in the encoder
num_states = 30  # Maximum number of states in the stateful transformer


model = StatefulTransformer(model_dim, num_heads, num_layers, model_dim, num_states)


loss_function = torch.nn.BCEWithLogitsLoss()
learning_rate = 0.001
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)


num_epochs = 10  # Number of epochs
reduce_learning_rate_at_epok = 10  # Reduce learning rate at this epoch
batch_size = 4  # Batch size
train_loss_list = []  # List to store training loss for each epoch
val_loss_list = []  # List to store validation loss for each epoch
epoch_run_time = []  # List to store the time it took to run each epoch
print("Starting training")
for epoch in range(num_epochs):
    print(f"Epoch {epoch + 1}/{num_epochs}")
    """Training loop"""
    model.train()  # Set model to training mode
    train_batch_loss = []  # Loss for each batch in the training loop
    start_time = dt.now()  # Start time for the epoch
    batch_outputs = torch.tensor(
        [], requires_grad=True
    )  # Outputs for each batch in the training loop
    batch_targets = torch.tensor([])  # Targets for each batch in the training loop

    for job_idx, job in enumerate(train_job_dataloader):
        print(f"Training job {job_idx + 1}/{len(train_job_dataloader)}", end="\r")

        output = model(job["input"])  # Forward pass

        batch_outputs = torch.cat(
            (batch_outputs, output), dim=0
        )  # Concatenate the outputs for each job
        batch_targets = torch.cat(
            (batch_targets, job["target"].expand_as(output)), dim=0
        )  # Concatenate the targets for the batch
        # Compute loss for the batch
        if (job_idx % batch_size == 0 and job_idx != 0) or job_idx == len(
            train_job_dataloader
        ) - 1:
            optimizer.zero_grad()  # Clear gradients
            loss = loss_function(batch_outputs, batch_targets)
            loss.backward()  # Backpropagation
            optimizer.step()  # Update weights

            train_batch_loss.append(
                loss.item()
            )  # Add the loss for the batch to the list
            batch_outputs = torch.tensor(
                [], requires_grad=True
            )  # Reset the outputs for the batch
            batch_targets = torch.tensor([])  # Reset the targets for the batch

    epok_loss = sum(train_batch_loss) / len(train_batch_loss)
    train_loss_list.append(epok_loss)
    print(f"\nTrain loss: {epok_loss}")

    """ Validation loop """
    model.eval()  # Set model to evaluation mode
    val_batch_loss = []  # Loss for each batch in the validation loop
    outputs = (
        []
    )  # Last prediction for each job in the validation loop (for confusion matrix)
    targets = []  # Target for each job in the validation loop (for confusion matrix)

    with torch.no_grad():
        for val_job_idx, val_job in enumerate(val_job_dataloader):
            print(
                f"Validation job {val_job_idx + 1}/{len(val_job_dataloader)}", end="\r"
            )

            output = model(val_job["input"])  # Forward pass

            val_loss = loss_function(
                output, val_job["target"].expand_as(output)
            )  # Compute loss
            val_batch_loss.append(
                val_loss.item()
            )  # Add the loss for the batch to the list

            # Extract target and average output for our confusion matrix
            average_output = torch.mean(output)
            outputs.append(average_output.item())
            targets.append(val_job["target"][-1].item())

        end_time = dt.now()  # End time for the epoch
        epoch_run_time.append(
            (end_time - start_time).total_seconds()
        )  # Add runtime for current epoch to list
        estimated_training_time = (
            sum(epoch_run_time) / len(epoch_run_time) * (num_epochs - (epoch + 1))
        )  # Calculate estimated training time
        estimated_training_time = format_seconds(
            estimated_training_time
        )  # Format the estimated training time
        current_val_loss = sum(val_batch_loss) / len(
            val_batch_loss
        )  # Calculate the validation loss for the epoch
        val_loss_list.append(current_val_loss)  # Add the validation loss to the list
        print(
            f"\nVal loss: {current_val_loss}. Estimated training time: {estimated_training_time}"
        )

    # Reduce the learning rate for the optimizer by a factor of 10 num epok > 2
    if epoch == reduce_learning_rate_at_epok:
        new_learning_rate = learning_rate / 10
        for param_group in optimizer.param_groups:
            param_group["lr"] = new_learning_rate

    # torch.save(model.state_dict(), f"models/job_transformer_epoch_{epoch}.pt")
    torch.save(model, f"models/job_transformer_single_input_epoch_{epoch}.pt")

print(f"Training time: {format_seconds(sum(epoch_run_time))}")


# plot training and validation loss
plt.figure()
plt.plot(train_loss_list)
plt.plot(val_loss_list)
plt.xlabel("epoch")
plt.ylabel("Loss")
plt.legend(["Training Loss", "Validation Loss"])
# plt.savefig("loss_plot.png")
plt.savefig("analysis_plots/loss_plot_single_input.png")


# Plot confusion
targets = [int(target) for target in targets]
limits = [0.90, 0.80]
visualizer().plot_confusion_matrix(
    targets,
    outputs,
    limits,
    "analysis_plots/confusion_matrix/confusion_matrix_single_input",
)
