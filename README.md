## Job Data Analyzer

The job data analyzer consists of three main components: data management, data analysis and machine learning.
This system depends on data stored via the Job and Site Collectors.

## Data Management

The Data Management is mainly done via two object: DataBox, which retrieves all site data related to a job exeuction, and DataTerminal, which essentially is a collection of DataBoxes.
These objects are responsible for all data operations related to a spesific job execution (DataBox), and operations for the whole dataset(DataTerminal). The Data Terminal is initialized by a collection of job attributes, which can be retrieved by the Job Data Handler. These properties are utilized to load the DataBoxes with grid site data corresponding to the given job exeuction properties. This is done by utilizing an influx manager, which populates predefined queries with job attributes to collect the corresponding site data. The sub-components related to Data Management is located in the data_managedment folder.

## Data Analysis

The Data Analysis component (run via data_analysis.py) is implemented as an interactive script which contains some predefined data analysis tasks. It acts as an intermediary between the DataTerminal (which performs the data operations) and a visualizer (which plots the data). This component can be utilized to create certain analysis plots and PDFs for a more comprehensive analysis of the collected data.

## Machine Learning

A machine learning model can be trained in this system by running the train_model.py script. This script, utilizes a custom dataset (JobDataset) and a model(StatefulTransformer), located in the ML_components folder, to train a custom ML model. The model did not perform very well, but the ML components serves as an example implementation for how a model can be implemented with the PyTorch libraries.
