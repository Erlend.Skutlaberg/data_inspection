import pandas as pd
from data_management.databox import DataBox
import data_management.influxManager as inf_man
import math
import numpy as np
import matplotlib.pyplot as plt
import json
import pickle
import os


class DataTerminal:

    def __init__(self, job_attributes, limit_buckets=False, ml_usage=False):
        self.stat_measures = ["mean", "median", "std", "unique_values"]
        self.databoxes = []
        self.job_attributes = job_attributes
        self.done_jobs = []
        self.error_jobs = []
        self.ml_usage = ml_usage

        with open("data_management/data_cleaning.json", "r") as cleaning_f:
            self.cleaning_conf = json.load(cleaning_f)

        with open("data_management/data_conf.json", "r") as data_f:
            data_conf = json.load(data_f)

        # Load buckets. If limit_buckets is True, we only load the focus_buckets
        self.buckets = data_conf["all_buckets"]
        if limit_buckets:
            self.buckets = data_conf["focus_buckets"]

        self.stat_df = pd.DataFrame()
        self.stats = pd.DataFrame()
        self.num_fields = {}

        self.load_boxes()

        self.create_data_matrix()

    # Print function that only prints if ml_usage is False
    def print(self, string):
        if not self.ml_usage:
            print(string)

    # Loads the databoxes for all job attributes in the DataTerminal
    def load_boxes(self):
        num_attr = len(self.job_attributes)
        for i in range(len(self.job_attributes)):
            self.print(f"Loading job {i+1}/{num_attr}")
            jobid, workernode, start_time, stop_time, end_status, _ = (
                self.job_attributes[i]
            )
            # Create a DataBox object for each job
            box = DataBox(
                jobid,
                workernode,
                start_time,
                stop_time,
                end_status,
                inf_man,
                self.stat_measures,
                self.buckets,
            )
            # Load data databox data (from InfluxDB)
            box.load_data()
            # Add to the done jobs or error jobs list
            if end_status.lower() == "done":
                self.done_jobs.append(box)
            else:
                self.error_jobs.append(box)
            # Append the DataBox to the DataTerminals collection of databoxes
            self.databoxes.append(box)

        self.print("Loading complete!")

    def filter_out_fields(self):

        for box in self.databoxes:
            cols_to_keep = self.cleaning_conf["cols_to_keep"]
            for bucket in cols_to_keep.keys():
                box.filter_out_fields(bucket, cols_to_keep[bucket])

    # Returns a list of the fields defined in the data_cleaning.json file
    def get_defined_fields(self):
        fields = []
        field_dict = self.cleaning_conf["cols_to_keep"]
        for bucket in field_dict:
            fields += field_dict[bucket]
        fields = sorted(fields)
        fields = ["execution_time"] + fields
        return fields

    # Creates the data matrix for each DataBox in the DataTerminal
    def create_data_matrix(
        self, log_vals=True, cols_to_keep=["execution_time", "_field", "_value"]
    ):
        for box in self.databoxes:
            if box.has_data():
                box.create_data_matrix()
                if box.data_matrix.empty:
                    continue

                box.data_matrix = box.data_matrix[cols_to_keep]
                # Pivot the df box.data_matrix such that each field becomes a column
                box.data_matrix = box.data_matrix.pivot_table(
                    index=["execution_time"], columns="_field", values="_value"
                )
                box.data_matrix = box.data_matrix.reset_index()
                box.data_matrix.fillna(box.data_matrix.mean(), inplace=True)
                # Logarithmize the defined fields (in addition to execution_time) if log_vals is True
                if log_vals:
                    # Logarithmize the defined fields
                    cols_to_log_dict = self.cleaning_conf["columns_to_log"]
                    cols_to_log_list = []
                    for bucket in cols_to_log_dict.keys():
                        cols_to_log_list += cols_to_log_dict[bucket]

                    for col in cols_to_log_list:
                        if col in box.data_matrix.columns:
                            box.data_matrix[col] = np.log(box.data_matrix[col] + 1)
                    box.data_matrix["execution_time"] = np.log(
                        box.data_matrix["execution_time"] + 1
                    )

    # Creates a correlation matrix, with the column header "_field" and "correlation_to_label"
    def get_field_to_label_corr_frame(
        self, features_col="_field", corr_col="correlation_to_label"
    ):

        for box in self.databoxes:
            box.data_matrix["job_id"] = [box.jobid] * len(box.data_matrix)
            box.data_matrix["label"] = [box.enc_label] * len(box.data_matrix)
        ml_frames = [box.data_matrix for box in self.databoxes]

        # Concatenate all data matrices into a single DataFrame
        concat_data_matrix = pd.concat(ml_frames, ignore_index=True)

        # Convert all values in each column to float
        for cols in concat_data_matrix.columns:
            # Convert all values in the rows to float
            concat_data_matrix[cols] = concat_data_matrix[cols].astype(float)

        # Print missing value. Missing values can only appear if a job miss a complete field, since they are imputed for each individual job (databox)
        self.print(f"Missing values: {concat_data_matrix.isna().sum().sum()}")
        self.print("Shape of the complete data matrix: ", concat_data_matrix.shape)

        # Impute NaN values with the mean of each column
        concat_data_matrix.fillna(concat_data_matrix.mean(), inplace=True)

        # Calculate the correlation matrix
        correlation_matrix = concat_data_matrix.corr()

        # Extract the correlation values between each feature and the label
        feature_label_correlation = correlation_matrix["label"].drop("label")

        # Sort the features based on the absolute correlation values
        sorted_features = feature_label_correlation.abs().sort_values(ascending=False)

        # Convert sorted_featrues to a dataframe
        sorted_features_df = sorted_features.reset_index().rename(
            columns={"index": features_col, "label": corr_col}
        )
        # Replace NaN values with 0 in the correlation column
        sorted_features_df[corr_col] = sorted_features_df[corr_col].fillna(0)
        # Return the name of the features and the correlation columns, as well as the correlation dataframe
        return sorted_features_df, features_col, corr_col

    # Groups all field values in the DataTerminal in a dictionary with the keys<field, [field values]> and splits them into done and error jobs dictionaries
    def group_field_measurements_and_split_on_end_status(self):
        fields = self.get_defined_fields()
        err_data = {f: [] for f in fields}
        done_data = {f: [] for f in fields}

        # Add all field values for done jobs to the done dictionary
        for i in range(len(self.done_jobs)):
            data = self.done_jobs[i].ML_data
            exec_time = data["execution_time"]

            for f in fields:
                if f in data.columns:
                    # Extracts the columns
                    f_data = data[f].tolist()
                    # Add the column (field values) to the done_data dictionary, under its field name
                    done_data[f].append((exec_time, f_data))
                else:
                    done_data[f].append(([], []))

        # Add all field values for error jobs to the error dictionary
        for i in range(len(self.error_jobs)):
            data = self.error_jobs[i].ML_data
            exec_time = data["execution_time"].tolist()

            for f in fields:
                if f in data.columns:
                    # Extracts the columns
                    f_data = data[f].tolist()
                    # Add the column (field values) to the err_data dictionary, under its field name
                    err_data[f].append((exec_time, f_data))
                else:
                    err_data[f].append(([], []))

        return done_data, err_data
