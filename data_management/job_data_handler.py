import requests
import json
import visualizer as v
from datetime import datetime

# API URLS. The Job Data Handler currently only utilizes the DBManager in the collector
urls = {"dbMan": "http://127.0.0.1:5001"}


# Get request for get requests to the DBManager
def get_data(url_key, endpoint, json_data):
    if url_key in urls.keys():
        url = urls[url_key]
    else:
        print("url-key not recognized")
        return ""

    try:
        res = requests.get(url + "/" + endpoint, json=json_data)

        if res.status_code == 200:
            data = json.loads(res.text)

            if "data" in data.keys():
                return data["data"]
        return ""
    except:
        return ""


# Post request for post requests to the DBManager
def post_data(url_key, endpoint, json_data):
    if url_key in urls.keys():
        url = urls[url_key]
    else:
        print("url-key not recognized")
        return ""

    try:
        res = requests.post(url + "/" + endpoint, json=json_data)

        if res.status_code == 200:
            data = json.loads(res.text)

            if "data" in data.keys():
                return data["data"]
        return ""
    except:
        return ""


# Get job attributes from the database of job properties that has been ensured that contains grid site data. Add the properties to their respective bucket list
def get_job_attributes_with_corresponding_site_data():
    cols = ["bucket", "jobid", "workernode", "startTime", "endTime", "label", "TTL"]
    table_name = "attr_with_site_data"
    json_data = {"table_name": table_name, "order_by": cols}
    rows = get_data("dbMan", "list_rows", json_data)

    slurm_rows = []
    self_node_rows = []
    system_nodes_rows = []
    for row in rows:
        if row[0] == "slurm":
            slurm_rows.append(row)
        elif row[0] == "self_nodes":
            self_node_rows.append(row)
        elif row[0] == "system_nodes":
            system_nodes_rows.append(row)

    return slurm_rows, self_node_rows, system_nodes_rows


# Return job attributes that are present in attr_list1 and attr_list2
def combine_attributes(attr_list1, attr_list2):
    combined_attr = []
    for attr1 in attr_list1:
        for attr2 in attr_list2:
            if attr1[1] == attr2[1]:
                combined_attr.append(attr1)
                break

    return combined_attr


# Split an attribute list on job end status
def split_attr_on_end_status(attr, index_of_end_status):
    done_attr = []
    err_attr = []
    for row in attr:
        if row[index_of_end_status].lower() == "done":
            done_attr.append(row[1:])
        elif row[index_of_end_status].lower() == "error_e":
            err_attr.append(row[1:])

    return done_attr, err_attr


# Create a table in the database for storing job attributes with corresponding site data
def create_attributes_with_site_data_table():

    existing_tables = get_data("dbMan", "list_tables", {})

    if not "attr_with_site_data" in existing_tables and len(existing_tables) > 0:
        # Create new table
        primary_keys = {"jobid": "VARCHAR(10)", "bucket": "VARCHAR(15)"}
        column_headers = {
            "jobid": "VARCHAR(10)",
            "bucket": "VARCHAR(15)",
            "workernode": "VARCHAR(50)",
            "startTime": "VARCHAR(30)",
            "endTime": "VARCHAR(30)",
            "label": "VARCHAR(10)",
            "TTL": "VARCHAR(30)",
        }
        json_data = {
            "table_name": "attr_with_site_data",
            "primary_keys": primary_keys,
            "column_headers": column_headers,
        }
        post_data("create_table", json_data)


# Get job attributes stored in the postgres database. FIlters our rows which is collected before the site collection started
def get_job_attributes(
    site_collection_start_time_str="2024-02-24 10:48:04",
    site_collection_end_time_str="2024-04-09 00:00:00",
    end_status_to_analyse=["done", "error_e"],
):
    psql_job_table_name = "jobparameters"

    cols = ["jobid", "workernode", "startTime", "endTime", "label", "TTL"]

    json_data = {"table_name": psql_job_table_name, "order_by": cols}

    rows = get_data("dbMan", "list_rows", json_data)

    # Datetime of when site collection started
    site_collection_start_time = datetime.strptime(
        site_collection_start_time_str, "%Y-%m-%d %H:%M:%S"
    )
    # Datetime of when site collection ended (ended at 2024-04-09)
    site_collection_end_time = datetime.strptime(
        site_collection_end_time_str, "%Y-%m-%d %H:%M:%S"
    )

    jobs = []
    for row in rows:
        jobid, workernode, start_time, end_time, label, TTL = row
        start_time = datetime.strptime(start_time, "%a, %d %b %Y %H:%M:%S %Z")
        end_time_dt = datetime.strptime(end_time, "%a, %d %b %Y %H:%M:%S %Z")
        if (
            len(workernode.split(".")) < 3
        ):  # Filter out jobs with workernode that does not contain a domain
            continue

        if (
            not jobid or not workernode or not start_time or not end_time or not label
        ):  # Filter out jobs with missing data
            continue

        if (
            not start_time > site_collection_start_time
            or start_time > site_collection_end_time
            or end_time > site_collection_end_time
        ):  # Filter out jobs that there are no site data for
            continue

        if (
            not label.lower() in end_status_to_analyse
        ):  # Filter out jobs with labels other than "done" and "error_e"
            continue
        start_time = start_time.strftime("%Y-%m-%d %H:%M:%S")
        jobs.append([jobid, workernode, start_time, end_time, label, TTL])

    return jobs
