import json
import pandas as pd
import numpy as np
from datetime import datetime, timedelta


np.seterr(divide="ignore")
pd.options.mode.chained_assignment = None  # default='warn'


class DataBox:
    def __init__(
        self,
        jobid,
        workernode,
        start_time,
        end_time,
        label,
        influx_man,
        stat_measures,
        buckets,
    ):
        # Assign job proerties to field values
        self.jobid = jobid
        self.workernode = workernode
        self.time_from = self.format_dt(start_time)
        self.time_from_dt = datetime.strptime(self.time_from, "%Y-%m-%dT%H:%M:%SZ")
        self.time_to = self.format_dt(end_time)
        self.label = label

        # Encode the job end status
        if label.lower() == "done":
            self.enc_label = 1
        else:
            self.enc_label = 0

        self.influx_man = influx_man
        self.data = {}
        self.buckets = buckets

        self.data_matrix = pd.DataFrame()

    # Load data from InfluxDB
    def load_data(self):
        start_interval, end_interval = self.extend_interval(
            self.time_from, self.time_to
        )

        for dt in self.buckets:
            df = self.influx_man.get_data(
                dt, self.jobid, self.workernode, start_interval, end_interval
            )

            if df.empty:
                continue

            self.data[dt] = self.extract_data_in_time_interval(df)

        self.add_execution_time_column()

    # Check if the DataBox has data
    def has_data(self):
        for bucket in self.buckets:
            if bucket in self.data.keys():
                if not self.data[bucket].empty:
                    return True
        return False

    # Check if the DataBox has a specific bucket
    def count_fields(self, bucket):
        if bucket in self.data.keys():
            return self.data[bucket]["_field"].nunique()
        else:
            return 0

    # Check if the DataBox has a specific bucket
    def get_data(self, data_type):
        if not data_type in self.data.keys():
            print(f"{data_type} is not a valid dataname")
            return None
        else:
            return self.data[data_type]

    # Check if the DataBox has a specific bucket
    def extract_data_in_time_interval(self, df):

        # Convert _time column to datetime objects
        df["_time"] = pd.to_datetime(df["_time"])

        # Extract rows between time_a and time_b
        filtered_df = df[
            (df["_time"] >= pd.Timestamp(self.time_from))
            & (df["_time"] <= pd.Timestamp(self.time_to))
        ]

        return filtered_df

    # Check if the DataBox has a specific bucket
    def format_dt(self, dt_str):

        date, time = dt_str.split(" ")
        return date + "T" + time + "Z"

    # Extends interval of a given start and end time by 1 hour
    def extend_interval(self, start, end):
        start_dt = datetime.strptime(start, "%Y-%m-%dT%H:%M:%SZ")
        end_dt = datetime.strptime(end, "%Y-%m-%dT%H:%M:%SZ")

        now = datetime.now()

        start_dt = start_dt - timedelta(hours=1)
        end_dt = end_dt + timedelta(hours=1)
        if end_dt > now:
            end_dt = now

        start_interval = start_dt.strftime("%Y-%m-%dT%H:%M:%SZ")
        end_interval = end_dt.strftime("%Y-%m-%dT%H:%M:%SZ")

        return start_interval, end_interval

    # Get unique values for a specific field
    def get_fields(self, bucket):
        if not bucket in self.data.keys():
            print(f"Bucket {bucket} not in databox.")
            return []

        if self.data[bucket].empty:
            print(f"Bucket {bucket} is empty.")
            return []

        df = self.data[bucket]

        return df["_field"].unique().tolist()

    # Get number of unique values for each field
    def get_unique_vals_for_each_field(self, bucket):
        if bucket not in self.data.keys():
            return {}
        if self.data[bucket].empty:
            return {}
        df = self.data[bucket]
        unique_fields = df["_field"].unique()
        field_nval_dict = {}
        # Iterate over each field and count the number of unique values
        for f in unique_fields:
            filtered_df = df[df["_field"] == f]
            nunique = filtered_df["_value"].nunique()
            field_nval_dict[f] = nunique
        return field_nval_dict

    # Removes the defined fields_to_remove from the given bucket
    def remove_fields(self, bucket, fields_to_remove):
        if bucket not in self.data.keys():
            return False
        if self.data[bucket].empty:
            return False
        df = self.data[bucket]

        df_filtered = df[~df["_field"].isin(fields_to_remove)]
        self.data[bucket] = df_filtered

        return True

    def filter_out_fields(self, bucket, fields_to_keep):
        if bucket not in self.data.keys():
            return False
        if self.data[bucket].empty:
            return False
        df = self.data[bucket]

        df_filtered = df[df["_field"].isin(fields_to_keep)]
        self.data[bucket] = df_filtered

        return True

    # Adds the exeuction time to the
    def add_execution_time_column(self):
        for bucket in self.data.keys():
            df = self.data[bucket]
            if df.empty:
                return False
            df["_time"] = df["_time"].dt.tz_localize(None)
            df = df.sort_values(by="_time")
            df["execution_time"] = (df["_time"] - self.time_from_dt).dt.total_seconds()

            self.data[bucket] = df

    def create_data_matrix(self, ttl_fraction_to_keep=1):
        self.add_execution_time_column()
        frames = []
        for bucket in self.data.keys():
            df = self.data[bucket]
            if not df.empty:
                frames.append(df)
        # frames = [df for df in self.data.values() if not df.empty]
        if not frames:
            return
        # Concatenate the data together
        if len(frames) == 1:
            data_matrix = frames[0]
        else:
            data_matrix = pd.concat(frames)
        if not "execution_time" in data_matrix.columns:
            return

        # Sort on execution_time
        data_matrix = data_matrix.sort_values(by="execution_time")

        # Get the biggest last time_used value
        last_time_used = data_matrix["execution_time"].max()

        # Calculate the time_used limit
        time_used_limit = last_time_used * ttl_fraction_to_keep

        # Remove all rows where time_used > time_used_limit
        data_matrix = data_matrix[data_matrix["execution_time"] <= time_used_limit]

        # Reset the index of the merged dataframe
        data_matrix.reset_index(drop=True, inplace=True)

        self.data_matrix = data_matrix
