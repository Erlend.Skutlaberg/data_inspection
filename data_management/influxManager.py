# Import required packages and access influxdb client
from datetime import datetime, timedelta

from collections import OrderedDict

from csv import DictReader

# import rx

# from rx import operators as ops

from dotenv import load_dotenv
import pandas as pd
import os

from influxdb_client import InfluxDBClient, Point, WriteOptions

from influxdb_client.client.write_api import SYNCHRONOUS
import warnings
from influxdb_client.client.warnings import MissingPivotFunction


warnings.simplefilter("ignore", MissingPivotFunction)

load_dotenv()

token = os.getenv("INFLUX_TOKEN")

org = os.getenv("ORG")

slurm_bucket = os.getenv("SLURM_BUCKET")
system_nodes_bucket = os.getenv("SYSTEM_NODES_BUCKET")
self_nodes_bucket = os.getenv("SELF_NODES_BUCKET")
jobrunner_nodes_bucket = os.getenv("SELF_JOBRUNNER_NODES_BUCKET")
jobagent_nodes_bucket = os.getenv("SELF_JOBAGENT_NODES")

buckets = [
    slurm_bucket,
    system_nodes_bucket,
    self_nodes_bucket,
    jobrunner_nodes_bucket,
    jobagent_nodes_bucket,
]

client = InfluxDBClient(url="http://127.0.0.1:8086", token=token, org=org)


def intersection(list1, list2):
    return list(set(list1) & set(list2))


# Function to query data from InfluxDB
def query_data(query):

    # Output the data as data frame named df

    df = client.query_api().query_data_frame(org=org, query=query)

    return df


# Get the keys from a bucket
def get_keys(bucket):
    q = f"""from(bucket: \"{bucket}\") |> range(start: -30m) |> keys()"""
    return query_data(q)


# Get data from the a bucket at a specific interval
def get_data_at_interval(bucket, time_from):
    q = f"""from(bucket: \"{bucket}\") |> range(start: {time_from})"""
    return query_data(q)


# Get data from the a bucket given the bucket name, as well as the job properties: jobid, worker node, and execution time interval
def get_data(bucket_name, jobid, workernode, time_from, time_to):

    if bucket_name == "job_agent":
        return get_jobagent_data(jobid, time_from, time_to)
    elif bucket_name == "slurm":
        return get_slurm_data(jobid, time_from, time_to)
    elif bucket_name == "self_nodes":
        return get_self_node_data(workernode, time_from, time_to)
    elif bucket_name == "system_nodes":
        return get_system_node_data(workernode, time_from, time_to)
    else:
        print("{bucket_name} is not a valid bucket_name")


# A predefined qeury for retrieving data from the slurm bucket. Populated with jobid and time interval
def get_slurm_data(jobid, time_from, time_to):

    q = f"""from(bucket: \"{slurm_bucket}\")                                                
    |> range(start: {time_from}, stop: {time_to})                                                    
    |> filter(fn: (r) => r["NodeName"] == \"{jobid}\")                                          
    """
    return query_data(q)


# A predefined qeury for retrieving data from the self node bucket. Populated with workernode and time interval
def get_self_node_data(workernode, time_from, time_to):

    q = f"""from(bucket: "{self_nodes_bucket}")
    |> range(start: {time_from}, stop: {time_to})
    |> filter(fn: (r) => r["NodeName"] == \"{workernode}\")                      
    """
    return query_data(q)


# A predefined qeury for retrieving data from the job agent bucket. Populated with workernode and time interval
def get_system_node_data(workernode, time_from, time_to):

    q = f"""from(bucket: "{system_nodes_bucket}")      
    |> range(start: {time_from}, stop: {time_to})                                         
    |> filter(fn: (r) => r["NodeName"] == \"{workernode}\")                                
    """
    return query_data(q)
