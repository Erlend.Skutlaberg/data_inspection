from data_management.dataTerminal import DataTerminal
import random
from visualizer.visualizer import Visualizer
import data_management.job_data_handler as jdh

visualizer = Visualizer()


def create_row_data(attr, bucket):
    row_data = {"bucket": "'" + bucket + "'"}
    for i, col in enumerate(cols):
        row_data[col] = "'" + attr[i] + "'"

    return row_data


def filter_and_save_job_data_params_which_has_site_data():
    rows = (
        jdh.get_job_attribues()
    )  # Get collected rows from the postgres jobparameters table
    jdh.create_attributes_with_site_data_table()  # Create a new table for the attributes with site data if it does not exist
    done_attributes, error_attributes = jdh.split_attr_on_end_status(
        rows, 4
    )  # Split the attributes on end status

    done_attributes = done_attributes[: len(error_attributes) * 2]
    print(f"Error attributes: {len(error_attributes)}")
    print(f"Done attributes: {len(done_attributes)}")

    num_jobs = len(jobs)
    attributes = done_attributes + error_attributes

    relative_attr_path = "data_attr_"
    relative_fields_path = "fields_in_"
    buckets = ["slurm", "self_nodes", "system_nodes"]

    for i, attr in enumerate(attributes):
        print(f"Analysing job {i+1}/{len(attributes)}")
        for bucket in buckets:
            term = DataTerminal([attr], buckets=[bucket])
            if term.databoxes[0].has_data():
                row_data = create_row_data(attr, bucket)
                json_data = {"table_name": "attr_with_site_data", "row_data": row_data}
                jdh.post_data("dbMan", "add_row", json_data)


def plot_comparison_of_defined_fields(num_jobs=500):
    slurm, self_nodes, system_nodes = (
        jdh.get_job_attributes_with_corresponding_site_data()
    )
    system_and_slurm_attr = jdh.combine_attributes(slurm, system_nodes)
    system__slurm_self_attr = jdh.combine_attributes(system_and_slurm_attr, self_nodes)
    done_attr, err_attr = jdh.split_attr_on_end_status(system__slurm_self_attr, 5)

    random.shuffle(err_attr)
    random.shuffle(done_attr)

    num_err_jobs = num_jobs // 2
    err_attr = err_attr[:num_err_jobs]
    done_attr = done_attr[:num_err_jobs]
    print(f"Comparing {num_err_jobs} error and {num_err_jobs} done jobs")
    analysis_attr = err_attr + done_attr

    # Load and prepare the data for analysis
    term = DataTerminal(analysis_attr, ["slurm", "system_nodes", "self_nodes"])
    term.filter_out_fields()
    term.create_ML_data(log_vals=False)

    fields = term.get_defined_fields()
    # Adds the
    done_data, err_data = term.group_field_measurements_and_split_on_end_status()

    for f in fields:
        visualizer.create_field_comparison_plot(
            done_data[f],
            err_data[f],
            f,
            f"analysis_plots/linear_plots/{f}_linear.png",
            "Field Value",
            "Execution time (seconds)",
            f"Comparison of field {f}",
        )
        visualizer.create_box_plot_of_error_and_done_jobs(
            done_data[f],
            err_data[f],
            f,
            f"analysis_plots/boxplots/{f}_boxplot.png",
            "Field Value",
            "Execution time (seconds)",
            f"Comparison of field {f}",
        )
    linear_pdf_path = "analysis_plots/linear.pdf"
    visualizer.plots_in_dir_to_pdf("analysis_plots/linear_plots", linear_pdf_path)
    boxplots_pdf_path = "analysis_plots/boxplots.pdf"
    visualizer.plots_in_dir_to_pdf("analysis_plots/boxplots", boxplots_pdf_path)

    print(f"Linear pdf saved to {linear_pdf_path}")
    print(f"Boxplot pdf saved to {boxplots_pdf_path}")


def plot_field_to_field_plot(x_field, y_field, num_jobs=500):
    print(f"Plotting comparison of {x_field} and {y_field}")
    slurm, self_nodes, system_nodes = (
        jdh.get_job_attributes_with_corresponding_site_data()
    )
    done_attr, err_attr = jdh.split_attr_on_end_status(slurm, 5)

    random.shuffle(err_attr)
    random.shuffle(done_attr)

    num_err_jobs = num_jobs // 2
    err_attr = err_attr[:num_err_jobs]
    done_attr = done_attr[:num_err_jobs]
    print(f"Comparing {num_err_jobs} error and {num_err_jobs} done jobs")
    analysis_attr = err_attr + done_attr

    # Load and prepare the data for analysis
    term = DataTerminal(analysis_attr, ["slurm"])
    term.filter_out_fields()
    term.create_ML_data(log_vals=False)

    fields = term.get_defined_fields()
    err_data = []
    done_data = []

    # Add each field to the corresponding list in done_jobs
    for i in range(len(term.done_jobs)):
        data = term.done_jobs[i].ML_data

        if x_field not in data.columns or y_field not in data.columns:
            continue
        x_data = data[x_field]
        y_data = data[y_field]
        done_data.append((x_data, y_data))

    # Add each field to the corresponding list in error_jobs
    for i in range(len(term.error_jobs)):
        data = term.error_jobs[i].ML_data

        if x_field not in data.columns or y_field not in data.columns:
            continue
        x_data = data[x_field]
        y_data = data[y_field]
        err_data.append((x_data, y_data))

    plot_path = (
        f"analysis_plots/field_to_field_comparison/{x_field}_{y_field}_comparison.png"
    )

    visualizer.create_field_comparison_plot(
        done_data,
        err_data,
        f,
        plot_path,
        x_field,
        y_field,
        f"Comparison of {x_field} and {y_field}",
    )

    print(f"Plot saved to {plot_path}")


def correlate_features_to_label(num_jobs=500):
    slurm, self_nodes, system_nodes = (
        jdh.get_job_attributes_with_corresponding_site_data()
    )

    buckets = ["slurm", "self_nodes", "system_nodes"]
    attribute_list = [slurm, self_nodes, system_nodes]
    for i, bucket in enumerate(buckets):
        attributes = attribute_list[i]
        done_attributes, error_attributes = jdh.split_attr_on_end_status(attributes, 5)

        done_attributes = done_attributes[0 : num_jobs // 2]
        error_attributes = error_attributes[0 : num_jobs // 2]

        analysis_attributes = done_attributes + error_attributes

        term = DataTerminal(analysis_attributes, [bucket])
        term.create_ML_data(log_vals=False)
        corr_frame, features_col, corr_col = term.get_field_to_label_corr_frame()

        path = f"analysis_plots/feature_label_corr_{bucket}"
        visualizer.save_plotted_hist(corr_frame, features_col, corr_col, bucket, path)
        print(f"Corr plot for fields in {bucket} saved in folder: {path}")


term = DataTerminal([])
fields = term.get_defined_fields()

while True:
    inp = input(
        "Choose a option: \n \
                1. Filter and save job data params which has site data\n \
                2. Plot comparison of defined fields\n \
                3. Correlate features to label\n \
                4. Create field to field plot \n \
                5.Exit\n"
    )
    if inp == "1":
        filter_and_save_job_data_params_which_has_site_data()
    elif inp == "2":
        num_jobs = input("Enter the number of jobs to compare: ")
        try:
            num_jobs = int(num_jobs)
            plot_comparison_of_defined_fields(num_jobs)
        except Exception as e:
            print(f"Invalid input! {e}")

    elif inp == "3":
        num_jobs = input("Enter the number of jobs to compare: ")

        try:
            num_jobs = int(num_jobs)
            correlate_features_to_label(num_jobs)
        except Exception as e:
            print(f"Invalid input! {e}")

    elif inp == "4":
        num_jobs = input("Enter the number of jobs to compare: ")
        print("Possible fields to compare: ")
        for f in fields:
            print(f"{fields.index(f)}. {f}")
        x_field = input("Enter the index of the x_field: ")
        y_field = input("Enter the index of the y_field: ")
        try:
            x_field = fields[int(x_field)]
            y_field = fields[int(y_field)]
            num_jobs = int(num_jobs)
        except Exception as e:
            print(f"Invalid input! {e}")
        plot_field_to_field_plot(x_field, y_field, num_jobs)

    elif inp == "5":
        break
    else:
        print("Invalid input")
