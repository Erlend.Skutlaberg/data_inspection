import torch
from torch.utils.data import Dataset
from data_management.dataTerminal import DataTerminal
import torch.nn.functional as F


# Dataset class for the job data. This class is used to create a dataset for the model
class JobDataset(Dataset):
    def __init__(self, attributes_list, buckets, input_size=0):
        self.attributes_list = attributes_list
        self.fields = None

        # Initialize fields and buckets
        term = DataTerminal([], ml_usage=True)
        self.fields = (
            term.get_defined_fields()
        )  # Get the fields from the data cleaning configuration
        self.buckets = buckets

        self.input_size = input_size

    # Returns the number of jobs in the dataset
    def __len__(self):
        return len(self.attributes_list)

    def __getitem__(self, idx):

        # Load job properties at position idx and create a DataTerminal object
        next_attr = self.attributes_list[idx]
        term = DataTerminal([next_attr], limit_buckets=True, ml_usage=True)
        term.filter_out_fields()
        term.create_ML_data()

        # Create input tensor and target tensor
        job_tensor = self.create_input_tensors_by_field_values(
            term.databoxes[0].data_matrix
        )

        label_tensor = torch.tensor(term.databoxes[0].enc_label, dtype=torch.float32)

        return {"input": job_tensor, "target": label_tensor}

    # Converts a data matrix (created by tehe databox) to a tensor (input tensor for the model)
    def create_input_tensors_by_field_values(self, df):
        tensors = (
            []
        )  # A list of tensors. Each tensor represents a row in the data matrix
        for i in range(len(df)):
            # Add all values from the data matrix rows to a input_list
            input_list = []
            for f in self.fields:
                if f in df.columns:
                    input_list.append(df[f][i])
                else:
                    input_list.append(0)
                # Pad the list to the correct size
                input_list = self.pad(input_list)
            # Convert the input_list to a tensor and add it to the tensor list
            tensors.append(torch.tensor(input_list, dtype=torch.float))
        # Stack the tensors to a single tensor
        input_tensors = torch.stack(tensors)

        return input_tensors

    # Pad the input tensor to the correct size
    def pad(self, inp):
        pad_amount = self.input_size - len(inp)
        # Pad the tensor to the correct size
        if pad_amount < 0:
            inp = inp[0 : self.input_size]

        elif pad_amount > 0:
            inp = inp + [0] * pad_amount

        return inp

    def set_input_size(self):
        self.input_size = len(self.fields)
