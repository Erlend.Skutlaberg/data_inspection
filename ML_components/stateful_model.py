import os
import torch
import torch.nn.functional as F

os.environ["OMP_NUM_THREADS"] = "5"
torch.set_num_threads(5)

import torch.nn as nn


# A stateful transformer model that uses a transformer model to classify job data. The model is stateful, meaning that it keeps track of the previous states, which in this case is previusly processed inputs
class StatefulTransformer(nn.Module):
    def __init__(
        self, d_model, nhead, num_layers, dim_feedforward, num_states, dropout=0.1
    ):
        super(StatefulTransformer, self).__init__()
        # Initiate a transformer model
        self.transformer = nn.Transformer(
            d_model=d_model,
            nhead=nhead,
            num_encoder_layers=num_layers,
            dim_feedforward=dim_feedforward,
            dropout=dropout,
        )
        self.d_model = d_model
        self.num_states = num_states
        self.state = None
        # Add a linear layer for binary classification
        self.linear_layer = nn.Linear(d_model, 1)

    def reset_state(self):
        self.state = None

    # Forward pass through the model.
    def forward(
        self, input_tensor, model_input_mask=None, model_input_key_padding_mask=None
    ):
        outputs = []
        self.reset_state()  # Reset the state for each forward pass
        # Loop through the input_tensor tensor list and extract each tensor
        for i in range(input_tensor.size(1)):
            model_input = input_tensor[:, i, :]

            if self.state is None:
                # Initial state is set to zeros*self.d_model
                self.state = torch.zeros(
                    self.num_states, self.d_model, device=model_input.device
                )

            # Forward pass through the transformer
            transformer_output = self.transformer(
                model_input,
                self.state,
                model_input_mask=model_input_mask,
                model_input_key_padding_mask=model_input_key_padding_mask,
            )

            # Add the last model_input to the state
            self.state = torch.cat((self.state, model_input), dim=0)
            # Reduce the first tensor in self.state if it contains more states than self.num_states
            if self.state.size(0) > self.num_states:
                self.state = self.state[-self.num_states :]
            # Average pool the transformer output, reduces the dimensionality
            pooled_transformer_output = F.avg_pool1d(
                transformer_output, kernel_size=transformer_output.size(0)
            ).squeeze()
            # Pass the output through the linear_layer to get a single value
            linear_out = self.linear_layer(pooled_transformer_output)
            # Add all linear outputs to a list
            outputs.append(linear_out)
        # Stack the linear outputs to a tensor
        output_tensor = torch.stack(outputs)
        # Pass all linear outputs through a Sigmoid activation function
        output_tensor = torch.sigmoid(output_tensor)
        return output_tensor
