import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
import os
import random
import torch
import numpy as np
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay


# Class that contains functions for visualizing data. The class contains functions for plotting histograms, confusion matrices, and other types of plots.
class Visualizer:

    def __init__(self):
        pass

    # Plots a histogram
    def save_and_plot_hist(self, field, values, column_x, column_y, bucket, path):
        plt.figure(figsize=(10, 6))
        plt.bar(field, values, color="skyblue")
        plt.xlabel(column_x)
        plt.ylabel(column_y)
        plt.title(f"Histogram of {column_y} by field for bucket {bucket}")
        plt.xticks(rotation=90)  # Rotate x-axis labels for better readability
        plt.tight_layout()  # Adjust layout to prevent clipping of labels
        plt.savefig(path)

    # Utilizes the save_and_plot_hist to create and save a histogram. If the number of fields is greater than 50, the function will split the fields in half and plot them separately.
    def save_plotted_hist(self, df, column_x, column_y, bucket, path):
        field = df[column_x].to_list()
        values = df[column_y].to_list()
        if len(field) > 50:

            self.save_and_plot_hist(
                field[0 : len(field) // 2],
                values[0 : len(values) // 2],
                column_x,
                column_y,
                bucket,
                path + ".png",
            )

            self.save_and_plot_hist(
                field[len(field) // 2 :],
                values[len(values) // 2 :],
                column_x,
                column_y + "_1",
                bucket,
                path + "_1" + ".png",
            )
        else:
            self.save_and_plot_hist(
                field, values, column_x, column_y, bucket, path + ".png"
            )

    # Function that plots the traiing and validation loss
    def plot_loss(self, train_loss_list, val_loss_list, path):
        plt.figure()
        plt.plot(train_loss_list)
        plt.plot(val_loss_list)
        plt.xlabel("epoch")
        plt.ylabel("Loss")
        plt.legend(["Training Loss", "Validation Loss"])
        plt.savefig(path)

    # Function that plots a confusion matrix, given a list of targets and outputs, and path and a list of limits, which are used to determine the threshold for the output (0 or 1)
    def plot_confusion_matrix(self, targets, outputs, limits, path):
        targets = [int(target) for target in targets]
        for limit in limits:
            predictions = [1 if val > limit else 0 for val in outputs]
            # Compute confusion matrix
            cm = confusion_matrix(targets, predictions)

            # Plot confusion matrix
            disp = ConfusionMatrixDisplay(confusion_matrix=cm)
            disp.plot(cmap=plt.cm.Blues)
            plt.title(f"Confusion matrix with limit {limit}")
            plt.savefig(path + f"_{limit}.png")
            plt.close()

    def plot_and_save_fields(self, df, title, filepath, all_possible_fields):
        plt.figure()

        color_palette = plt.cm.get_cmap("Set1")
        # Step 1: Get unique field names
        # unique_fields = df['_field'].unique()
        unique_fields = all_possible_fields

        # Step 2-4: Plot execution time against field value for each field name
        for i, field in enumerate(unique_fields):

            if field in df.columns:
                field_df = df[field]
            else:
                field_df = np.zeros(len(df))
            color = color_palette(i / len(unique_fields))  # Get color from the colormap

            plt.plot(df["time_used"], field_df, label=field, color=color)

        # Add labels and legend
        plt.xlabel("Execution Time")
        plt.ylabel("Field Value")
        plt.legend(
            title="fields",
            bbox_to_anchor=(1.05, 1),
            loc="upper left",
            borderaxespad=0.0,
        )
        plt.title(title)
        plt.savefig(filepath, bbox_inches="tight")
        plt.close()

    def folders_to_pdf(self, done_jobs_folder, error_jobs_folder, filepath):
        # Create a new PDF file
        with PdfPages(filepath) as pdf:
            done_files = os.listdir(done_jobs_folder)
            error_files = os.listdir(error_jobs_folder)
            num_pages = max(len(done_files), len(error_files))

            for i in range(num_pages):
                fig, axs = plt.subplots(2, 1, figsize=(10, 8))

                # Insert plots from done_jobs folder
                if i < len(done_files):
                    done_plot_path = os.path.join(done_jobs_folder, done_files[i])
                    axs[0].imshow(plt.imread(done_plot_path))
                    axs[0].axis("off")

                # Insert plots from error_jobs folder
                if i < len(error_files):
                    error_plot_path = os.path.join(error_jobs_folder, error_files[i])
                    axs[1].imshow(plt.imread(error_plot_path))
                    axs[1].axis("off")

                pdf.savefig(fig, bbox_inches="tight")
                plt.close(fig)

    def create_field_comparison_plot(
        self, done_jobs, error_jobs, field, path, x_col_name, y_col_name, title
    ):
        # All done_jobs in green, all error_jobs in red
        plt.figure()
        plt.title(title)

        for x_vals, y_vals in done_jobs:
            plt.plot(x_vals, y_vals, label=f"done", color="blue")
        for x_vals, y_vals in error_jobs:
            plt.plot(x_vals, y_vals, label="error", color="yellow")
        plt.ylabel(y_col_name)
        plt.xlabel(x_col_name)
        plt.savefig(path)

    def create_box_plot_of_error_and_done_jobs(
        self, done_jobs, error_jobs, field, path
    ):
        # All done_jobs in blue, all error_jobs in red
        plt.figure()
        plt.title(f"Comparison of field {field}")

        concat_done_jobs = []
        for _, job in done_jobs:
            concat_done_jobs += job
        concat_error_jobs = []
        for _, job in error_jobs:
            concat_error_jobs += job
        # Calculate average v
        plt.boxplot(concat_done_jobs, positions=[0], labels=["done"])
        plt.boxplot(concat_error_jobs, positions=[1], labels=["error"])
        # Set y axis label for the fig
        plt.ylabel("Field value")
        plt.savefig(path)

    def plots_in_dir_to_pdf(self, dir, save_path):
        # Create a new PDF file
        with PdfPages(save_path) as pdf:
            files = os.listdir(dir)
            for file in files:
                fig = plt.figure()
                plot_path = os.path.join(dir, file)
                plt.imshow(plt.imread(plot_path))
                plt.axis("off")
                pdf.savefig(fig, bbox_inches="tight")
                plt.close(fig)
        # Remove the plots in the folder
        for file in files:
            try:
                os.remove(os.path.join(dir, file))
            except:
                continue
